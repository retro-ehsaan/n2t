<!DOCTYPE HTML>
<html>
	<head>
		<title>N2T − کوتاه کننده لینک</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		
		<link rel="stylesheet" href="css/reset.css" type="text/css">
		<link rel="stylesheet" href="css/style.css" type="text/css">
		
		<link rel="shortcut icon" href="img/favicon.png" type="image/png">
		<link rel="apple-touch-icon" href="img/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">
		
		<meta name="description" content="N2T به شما کمک می کند تا ظرف چند دقیقه لینک خود را به راحتی کوتاه کنید و از آن لذت ببرید!">
		<meta name="keywords" content="n2t.ir,کوتاه کننده لینک,لینک کوتاه,ن تو تی">
		<meta name="robots" content="index,follow">
	</head>
	<body>
		<div class="overlay"><span></span></div>
		<div class="container">
			<h1>کوتاه کننده لینک</h1>
			<form action="#short" method="POST" id="shortform" autocomplete="off">
				<input type="url" name="url" id="url" placeholder="http://" autocomplete="off" required>
				<input type="submit" name="submit" id="submit" value="کوتاه کن!">
			</form>
		</div>
		<div class="nav">
			<span class="api"><a href="api.php"><img src="img/api.png" alt="API"></a></span>
			<span class="blog"><a href="blog/" target="_blank"><img src="img/blog.png" alt="وبلاگ"></a></span>
		</div>
		<script src="//code.jquery.com/jquery.min.js" type="text/javascript"></script>
		<script src="js/init.js" type="text/javascript"></script>
	</body>
</html>