<!DOCTYPE HTML>
<html>
	<head>
		<title>API − N2T</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="css/responsive.css">
		<link rel="stylesheet" type="text/css" href="css/api.css">
		
		<meta name="robots" content="index,follow">
		<meta name="description" content="N2T یک API فوق العاده آسان و بهینه برای متصل شدن به برنامه های شما دارد!">
		<meta name="keywords" content="N2T,API,ای پی آی,API کوتاه کننده لینک">
	</head>
	<body>
		<div class="container">
			<h1>API کوتاه کننده لینک</h1>
			<p>کوتاه کننده لینک N2T امکانات ویژه&zwnj;ای برای برنامه&zwnj;نویسان وب فراهم آورده است.</p>
			<p>آدرس بیس اپ کوتاه کننده لینک،در زیر ذکر شده است</p>
			<code>http://n2t.ir/app/short.php</code>
			<p>برای ارسال URL از روش POST و با پارامتر URL عمل کنید. نمونه درخواست در زیر آورده شده است.</p>
			<code>Param: url - String(300)</code>
			<code>POST http://n2t.ir/app/short.php , url=http://test.ir</code>
			<p>در هنگام بازگشت، N2T یک نتیجه به صورت JSON ارسال می&zwnj;شود که کلید error در هر صورت وجود دارد.</p>
			<p>در صورتی که کلید error برابر با 0 باشد،  لینک کوتاه شده در کلید shorted وجود خواهد داشت. مانند زیر</p>
			<code>{error:0,shorted:http://n2t.ir/12345}</code>
			<p>ولی در صورتی که مقدار error با 1 برابری کند، پارامتر reason وجود دارد. مانند زیر</p>
			<code>{error:1,reason:invalid_url}</code>
			<p><strong>هــــمـــیـــن!</strong></p>
			<p>آپدیت و خبر نسخه&zwnj;های جدید را در <a href="/blog">وبلاگ</a> مطالعه کنید.</p>
		</div>
	</body>
</html>